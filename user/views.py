from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import user_form
from django.contrib.auth.models import User

from .models import Profile


def signup(request):
    if request.method == "POST":
        data = request.POST
        user = User.objects.create_user(username=data['username'], password=data['password1'])
        user.save()
        user = authenticate(request, username=data['username'], password=data['password1'])
        if user is not None:
            form = login(request, user)
            return redirect("/create_profile")
    return render(request, 'user/signup.html')


def create_profile(request):
    if request.method == 'POST':
        data = request.POST
        form = user_form(data)
        if form.is_valid():
            form.save()
        return redirect('/admin')
    return render(request, "user/create_profile.html", {"form": user_form})


def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            form = login(request, user)
            if request.session.has_key('username'):
                request.session['username'] = username
            return redirect('/get_books')
    form = AuthenticationForm()
    return render(request, 'user/login.html', {'form': form, 'title': 'log in'})




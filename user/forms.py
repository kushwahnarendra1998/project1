from django.forms import ModelForm
from .models import Profile


class user_form(ModelForm):
    class Meta:
        model = Profile
        fields = '__all__'

from django.urls import path
from . import views

urlpatterns = [
    path("signup/", views.signup),
    path("create_profile/", views.create_profile),
    path("login/", views.login_view),
]

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import AddBooks
from .models import Book


@login_required
def add_books(request):
    if request.method == "POST":
        data = request.POST
        form = AddBooks(data)
        if form.is_valid():
            form.save()
            return redirect("/get_books")
    return render(request, "books/add_books.html", {'form': AddBooks})


@login_required
def get_books(request):

    if request.method == "GET":
        data = Book.objects.all()
        return render(request, "books/get_books.html", {'form': data})


@login_required
def update_books(request, pk):
    data = Book.objects.filter(pk=pk).first()
    form = AddBooks(request.POST or None, instance=data)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/get_books")
    return render(request, "books/update.html",{'form':form})


@login_required
def destroy(request, pk):
    data = Book.objects.get(pk=pk)
    data.delete()
    return redirect("/get_books")


def home(request):
    if request.method == "GET":
        data = Book.objects.all()
        return redirect("/get_books")
    return render(request, "books/homepage.html")

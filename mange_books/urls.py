from django.urls import path
from .views import get_books, update_books, destroy, add_books, home

urlpatterns = [
    path('', home),
    path("get_books", get_books),
    path("edit/<int:pk>", update_books),
    path("destroy/<int:pk>", destroy),
    path("add_books", add_books),

]
